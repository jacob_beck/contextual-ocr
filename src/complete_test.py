#! /usr/bin/python

"""
roi_test.py - Testing out text detection
Jacob Beck - beckjac@oregonstate.edu
"""

import cv2
import numpy as np
from PIL import Image as PyImage
import pyocr

def find_roi(img):
    # Find MSER
    mser = cv2.MSER()
    regions = mser.detect(img, None)

    # Extract hulls of regions
    hulls = [cv2.convexHull(r) for r in regions]
    raw = [h.reshape(-1, 2) for h in hulls] # Make indexing less stupid

    # Filter properties
    ellipses = []
    for r in raw:
        if len(r) >= 5:
            ellipses.append(cv2.fitEllipse(r)) # center, axes, angle

    median_size = np.median([np.linalg.norm(e[1]) for e in ellipses])

    filtered = []
    for e in ellipses:
        center, axes, angle = e[:]
        
        # Reject regions that are too small
        if axes[0] < 8:
            continue
        
        # Reject regions that are too skinny
        aspect_ratio = axes[0]/axes[1] # minor/major
        if aspect_ratio < 0.1:
            continue
            
        # Reject regions that are too big
        if np.linalg.norm(axes) > median_size*1.5:
            continue
        
        # Add regions that pass
        filtered.append(e)

    # Create binary image to find bounding boxes
    bin_img = np.zeros(img.shape[:2], dtype=np.uint8)
    for f in filtered:
        center, axes, angle = f[:]
        center = tuple([int(round(x)) for x in center])
        axes = tuple([int(round(x)) for x in axes])
        angle = int(round(angle))
        
        cv2.ellipse(bin_img, center, axes, angle, 0, 360, (255, 255, 255), -1)

    cv2.imshow('bin', bin_img)

    # Find bounding boxes
    contours, hierarchy = cv2.findContours(bin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    boxes = []
    for c in contours:
        boxes.append(cv2.boundingRect(c))

    # Remove redundant boxes
    boxes.sort(key=lambda x: x[2]*x[3], reverse=True)

    final = []
    for b in boxes:
        center = (b[0]+b[2]/2, b[1]+b[3]/2)
        
        contained = False
        for f in final:
            left = f[0]
            right = left + f[2]
            up = f[1]
            down = up + f[3]
            
            if (center[0] >= left and center[0] <= right) and (center[1] >= up and center[1] <= down):
                contained = True
                break
        
        if not contained:
            final.append(b)
    
    return final
    
def extract_roi(img, bounding_boxes):
    regions = []
    
    # Iterate through ROI
    for bbox in bounding_boxes:
        pt1 = (bbox[0], bbox[1])
        pt2 = (bbox[0]+bbox[2], bbox[1]+bbox[3])
        
        # Extract sub-image
        local_img = img[pt1[1]:pt2[1], pt1[0]:pt2[0]]
        
        regions.append([local_img, pt1, pt2])
    
    return regions

def run_ocr(vis, rois):
    # Iterate through images
    for region in rois:
        img = region[0]
        start = region[1]
        end = region[2]
        
        # Convert to PIL
        pil_img = PyImage.fromarray(img)

        # Find text
        ocr = pyocr.get_available_tools()[0]
        word_boxes = ocr.image_to_string(
            pil_img,
            lang='eng',
            builder=pyocr.builders.WordBoxBuilder()
        )

        # Show located text
        for wbox in word_boxes:
            word = None
            try:
                word = str(wbox.content)
            except UnicodeEncodeError:
                word = "(Non ASCII)"
            
            if word.isspace():
                continue
            
            print "Found: ", word
            
            # Convert from local coordinates
            pt1 = (wbox.position[0][0] + start[0], wbox.position[0][1] + start[1])
            pt2 = (wbox.position[1][0] + start[0], wbox.position[1][1] + start[1])
            
            # Annote image
            cv2.rectangle(vis, pt1, pt2, (0,255,0))
            cv2.putText(vis, word, pt1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0))
        
def main():
    img = cv2.imread('sign.jpg')
    vis = img.copy()
    img = cv2.cvtColor(vis, cv2.COLOR_BGR2GRAY)
    
    # Preprocess
    blur = cv2.GaussianBlur(img,(9,9),0)
    
    # Localize text
    #~ boxes = [[0,0,img.shape[0],img.shape[1]]] # Entire image
    boxes = find_roi(blur)
    rois = extract_roi(img, boxes)
    
    # Read text
    run_ocr(vis, rois)

    cv2.imshow('final', vis)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
